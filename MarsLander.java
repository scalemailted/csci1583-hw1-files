/*Objectives - 01.) read the scene width & height         (int, int)
               02.) read the background image             (text)
               03.) read the ship art assets              (text...)
               04.) read the ship width/2 & height/2      (int, int)
               05.) read the ship x,y position            (double, double)
               06.) read the fuel amount                  (int)
               08.) read the gravity amount               (double)
               09.) read the max survivable velocity      (double)
               10.) read the thrust amount                (double)
               11.) read the thrust x location & width/2  (int, int)

execute: java MarsLander < level0.txt
*/
